<?php
/* @var $this DefaultController */
/* @var $Facebook_name DefaultController */
/* @var $Twitter_name DefaultController */

$this->pageTitle ='Timelines';
$this->breadcrumbs=array(
	$this->module->id,
);
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
<section class="content">
	<div class="row">
		<div class="col-sm-4" >
			<h2 style="border-bottom:1px solid black;">Facebook Timeline</h2>
		<center>
				<div  class="fb-page" data-href="https://www.facebook.com/<?php echo $Facebook_name; ?>" data-tabs="timeline" data-width="400" data-height="800" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false" >
				<blockquote  cite="https://www.facebook.com/<?php echo $Facebook_name; ?>" class="fb-xfbml-parse-ignore">
					<a href="https://www.facebook.com/<?php echo $Facebook_name; ?>" ></a>
				</blockquote>
			</div>
			</center>
		</div>
		<div class="col-sm-4" style="max-height: 800px;max-width:500px;overflow: auto;">
				<h2 style="border-bottom:1px solid black;">Twitter Timeline</h2>
			<a class="twitter-timeline" href="https://twitter.com/<?php echo $Twitter_name; ?>">Tweets by <?php echo $Twitter_name; ?></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		</div>
		<div class="col-sm-4" style="max-height: 800px;max-width:500px;overflow: auto;">
			<h2 style="border-bottom:1px solid black;">Instagram Timeline</h2>

			<script type="text/javascript">
				var userFeed = new Instafeed({
					get: 'user',
					userId: '<?php echo $instagram_user_id ?>',
					resolution:'standard_resolution',
					accessToken: '<?php echo $instagram_accessToken ?>',
					template: '<div class="col-sm-12"><a class="animation" href="{{link}}" target="_blank"><img src="{{image}}" class="img-thumbnail img-responsive" style="width:100%;"/></a><h4>Comments : <span style="color:blue;">{{comments}}</span> &nbsp;&nbsp;Likes : <span style="color:blue;">{{likes}}</span> </h4><h4>@Emaratalyoum   <small style="color:black;">{{caption}}</small></h4></div>'
				});
				userFeed.run();
			</script>

			<div id="instafeed"></div>
		</div>
	</div>
</section>